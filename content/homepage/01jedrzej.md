---
title: "Jędrzej Derouiche"
date:
weight: 1
background: ""
avatar: "/images/jedrzej.jpg"
align:
---

Studiuję fotografię intermedialną oraz terapię Gestalt. Interesuje mnie kino dokumentalne, które układa przypadek w opowieści o nas samych. Ostatnio w wolnych chwilach maluję farbami olejnymi i piję mrożoną kawę.
