---
title: "Ida Bednarek"
date:
weight: 2
background: ""
avatar: "/images/ida.jpg"
align:
---

Psychoterapeutka Gestalt w trakcie szkolenia i psycholog.

Na co dzień pracuję w szpitalu klinicznym imienia Józefa Babińskiego w Krakowie. Prowadzę również procesy psychoterapeutyczne dorosłych i młodzieży w Centrum Zdrowia Psychicznego w Dal oraz we własnym gabinecie. Doświadczenie zdobywałam podczas stażów na oddziałach psychiatrycznych oraz różnych szkoleń i treningów ogólnorozwojowych. Od lat moją pasją jest praca z ludźmi. Bycie obecnym w tych trudnych i pięknych momentach, z drugim człowiekiem, daje mi energię do życia. Cenię siłę jaką ma w sobie grupa. Interakcje, wzajemne wsparcie i przeżycia mają, moim zdaniem, wartość leczącą i rozwojową dla każdej jednostki. Bardzo lubię również gry planszowe, yogę, oraz poranną kawę 😉
