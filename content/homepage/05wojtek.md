---
title: "Wojtek Marczewski"
date:
weight: 5
background: ""
avatar: "/images/wojtek.jpg"
align:
---

Terapeuta Gestalt, miłośnik czworonogów i spacerów nad mazurskimi jeziorami. Na co dzień pracuję z dziećmi i młodzieżą. Wielbiciel Warmi i Mazur. Mistrz Gry w Warhammmer 40k. Doktor filozofii z dużym dystansem do samego siebie.