---
title: "Basia Stańko-Jurczyńska"
date:
weight: 3
background: ""
avatar: "/images/basia.jpg"
align:
---

Energiczna i spontaniczna studentka Rzeźby na Uniwersytecie artystycznym w Poznaniu. Sztuka to jej pasja i sposób na życie. Prowadzi warsztaty dla młodzieży. W wolnych chwilach szuka nowej stylizacji na upalne dni.