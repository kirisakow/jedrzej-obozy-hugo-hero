---
title: "Paweł Krzysztof Kasprzak"
date:
weight: 4
background: ""
avatar: "/images/pawel-krzysztof.jpg"
align:
---

Na co dzień nauczyciel w Technikum fotograficznym. Ogarnia Premiera i Photoshopa. Rozwijający się artysta i profesjonalista. Nasz Mistrz gry w „Tajemnice Pętli” od wydawnictwa Blacko Monk. Gitarzysta w zespole Alarm. Czasem śmiertelnie poważny, zawsze z uśmiechem na twarzy. Nie przegapcie jego koncertu!