---
title: "Nasze półki"
date:
heroHeading: ""
heroSubHeading: ""
heroBackground: ""
---

{{< figure width="100%" src="gre.jpg" caption="" >}}

W naszej ofercie proponujemy Ci wieczory z trzema znanymi planszówkami. Na postawie ankiety zostałeś przydzielony do odpowiedniej dla Ciebie grupy.

### Etherfields

Etherfields czyli kraina snów. To tę grę oferujemy Ci w dwóch wersjach językowych. Wspólnie będziemy poznawać zasady panujące w tej krainie snów i eksplorować wymyślony przez autorów świat.

### Tainted Grail

Tainted Grail czyli dla śmiałków, którzy chcą się przenieść w Karinę dziwów oraz stoczyć bitwy z groźnymi potworami i walczyć z pierwszymi. Idealny świat dla fanów legend wiktoriańskich

### Tajemnice Pętli

> ZAGRAJ W LATACH 80., KTÓRYCH NIGDY NIE BYŁO! | Wejdźcie w role w rezolutnych Dzieciaków i przeżyjcie masę ciekawych przygód w alternatywnej rzeczywistości lat 80. XX wieku! System pozwoli Wam odkrywać niezwykłe tajemnice związane z zaawansowaną technologią i ukaże nowe oblicze Stanów Zjednoczonych, Szwecji albo peerelowskiej Polski. Jeżeli jesteście fanami Stranger Thingsalbo Scooby Doo, na pewno się tu odnajdziecie!

<p class="text-right pb-2">— opis wydawcy</p>

> „Niebezpiecznie wychodzić za własny próg, mój Frodo! Trafisz na gościniec i jeżeli nie powstrzymasz swoich nóg, ani się spostrzeżesz, kiedy cię poniosą”.

<p class="text-right pb-2">— Bilbo Baggins</p>

### Łowcy A. D. 2114

> Łowcy A. D. 2114 to kooperacyjna gra planszowa, w której wraz z przyjaciółmi wcielasz się w łowców maszyn. Weźmiecie udział w rozbudowanej i wciągającej kampanii, będziecie rozwijać własne schronienie, handlować, mierzyć się z wrogami podczas kilkudziesięciu taktycznych misji, wykonywać zlecenia oraz odwiedzać unikalne lokacje.

<p class="text-right pb-2">— opis autora</p>

### Scythe

> Scythe to gra idealnie łącząca ze sobą mechaniki znane z eurogier z mocno klimatyczną rozgrywką. Zachwyci zarówno graczy, którzy lubią rozwój, optymalizację i strategiczne planowanie, jak również tych, którzy cenią w grach wyraźną warstwę fabularną, elementy przygodowe, powiązanie zasad gry z tematem i ograniczenie negatywnej interakcji.

<p class="text-right pb-2">— opis wydawcy</p>

### Pluszowe Оpowieści

> Pluszowe opowieści to zupełnie nowy format gry zaklętej w księgę przygód. Rozgrywka toczy się nie na planszy, a specjalnej księdze, która skrywa wiele tajemnic. Z każdą kolejną kartą, coraz bardziej zagłębicie się w fascynującym świecie Pluszowych Opowieści!

<p class="text-right pb-2">— opis wydawcy</p>

### Opowieści z Pryncypii

> Opowieści z Pryncypii: Królewna Łucja i obrońcy królestwa to ekscytująca gra narracyjna oparta na współpracy. Uczestnicy wcielają się w niej w chowańce – ich zadaniem będzie ocalenie i wychowanie następczyni tronu. Muszą ją chronić przed siłami zła, które pragną jej śmierci, ale przecież dziecku potrzeba czegoś więcej aniżeli tylko ocalenia! Chowańce wiedzą, że każda decyzja, którą podejmą, wpłynie na przyszłość powierzonej im dziewczynki oraz na to, jaką będzie władczynią, jeśli odzyska tron.

<p class="text-right pb-2">— opis wydawcy</p>

X

{{< figure width="100%" src="190404_Michalowice_6478.jpeg" caption="Sala warsztatowa, z jeleniem widzialnym w lusterku i niedźwiedziem koło kominka." >}}
