---
title: "Oferta"
date:
heroHeading: ""
heroSubHeading: ""
heroBackground: ""
---

## *Przewodnim motywem naszych obozów są gry planszowe! Proponujemy podróż w świat z popularnymi planszówkami — Etherfields, Tainted Grail i wiele innych*

{{< figure width="100%" src="180905_Obecnosc_9814.jpeg" caption="Opis obrazka, opis obrazka, opis obrazka." >}}

Na naszych obozach stawiamy przede wszystkim na indywidualny rozwój każdego z uczestników. Obozowicz nie znajdzie u nas planu lekcji czy listy obowiązków. Wszechstronnie wykształcona i otwarta kadra zapewnia szeroki wybór różnych aktywności dostosowanych do potrzeb i upodobań młodzieży. Staramy się ukierunkować młodych ludzi na rozwijanie swojego poczucia własnej sprawczości, autonomii i odkrywania własnych potrzeb. To wszystko w duchu wzajemnej empatii, ciepła, opieki i [gier planszowych](/gre)!

### TERMINY

Przygoda trwa od 2 sierpnia do 16 sierpnia 2023r. Cena to 3 349 zł wraz z wyżywieniem. Częstujemy Was jadłospisem wegetariańskim 🙂 Zapraszamy Was do zjawiskowego miejsca przy ulicy ul. Kolonijna 8 w Piechowicach na połudnowym zachodzie Polski. Przywita Was duży domek z ogrodem. Idealne miejsce do eksplorowania magicznego świata naszych gier, warsztatów i odpoczynku. My już nie możemy się Was doczekać, a wy?

### PSYCHOEDUKACJA

Z uwagi na to, jak ważny jest dla Nas Twój komfort, podczas pobytu na naszych obozach nie może Ciebie zabraknąć na tych zajęciach. Warsztaty odbywają się codziennie o tej samej godzinie. Są prowadzone przez psycholożkę i psychoterapeutkę Gestalt. W trakcie takich zajęć polepszysz swoje funkcjonowanie psychiczne i interpersonalne. Dowiesz się więcej na temat swoich uczuć i potrzeb oraz wspólnie będziemy szukać drogi do zaprzyjaźnienia się z nimi. Będziesz również miał możliwość dowiedzenia się więcej na temat mechanizmów działania Twojej psychiki i nauczysz się użytecznego korzystania z nich.

### AKTYWNOŚCI SPORTOWE

Lato sprzyja chęci przebywania na świeżym powietrzu. Każdy wie, że ruch polepsza nasze funkcjonowanie pod każdym względem, a przecież ważne jest, by Twój umysł funkcjonował sprawnie podczas wielogodzinnych wieczornych rozgrywek! Będziesz mógł uczestniczyć w aktywnościach sportowych takich jak: poranny jogging, gra w koszykówkę czy siatkówkę, badminton czy inne aktywności na jakie wyrazisz chęć. Jesteśmy otwarci na Twoją kreatywność 😉

### ZEBRANIE SPOŁECZNOŚCI

Są to codzienne zebrania Nas wszystkich, które prowadzi kadra obozowa, podczas tych spotkań macie możliwość wyrażenia swoich potrzeb i preferencji co do aktywności w naszej ofercie. Jest to czas, gdzie jako uczestnicy możecie uruchomić kreatywną część siebie wchodząc w dyskusję i proponując różne pomysły na temat naszego wspólnego czasu.
