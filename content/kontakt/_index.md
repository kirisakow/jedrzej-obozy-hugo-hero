---
title: 'Kontakt'
date:
---

Jeśli jesteś zainteresowany, chcesz się zapisać, masz pytania czy wątpliwości.  Zapraszamy do kontaktu z naszym koordynatorem.

Dane kontaktowe poniżej:

* Jędrzej Derouiche
* Tel. 889 732 504
* Mail: jedrzejderouiche, at gmail

